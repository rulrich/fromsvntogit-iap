#!/usr/bin/python3

with open('authors-svn.txt', 'r') as repo, \
     open('authors-devel-ik.txt', 'r') as devel, \
     open('authors.txt', 'w') as out:

    repo_users={}
    for line in repo:
        words = line.split('=')
        user = words[0].strip()
        repo_users[user] = line
    repo.close()
        
    for line in devel:
        words = line.split('=')
        user = words[0].strip()
        if (user in repo_users):
            del repo_users[user]
        out.write(line)
    devel.close()

    for repo_user in repo_users:
        out.write(repo_users[repo_user])
    out.close()
