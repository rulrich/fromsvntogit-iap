# Preface

This twiki was useful for our local migration from svn to git. 

# Status, as after July 2020

The devel-ik.fzk.de server is in read-only mode. There is no guarantee for accessibility after July 2020. It will disappear soon. All data is migrated to 
`/cr/users/svn/repositories` and can be access with a valid IKP computer account (or ssh key, if absolutely needed). 

You can migrate your existing local checkouts like `svn relocate file:/cr/users/svn/repositories/XXXX` and also all svn:externals can be adapted (manually) to the new repository location.  

However, the default solution is to migrate all development and work to git/gitlab. 


# Description and migration

It mainly assumes the transfer of projects from devel-ik.fzk.de/svn to gitlab.ikp.kit.edu with all history and authorship data. 

The advantage of using `git-svn` as outlined here is that for a transition period it is possible to continue to use **both** git and svn in parallel without much trouble. 

However, it is not the ideal scenario to run git and svn in parallel over a longer time. It is much better, and encouraged, to fully migrate to git asap. 

The authorship data needed for parts of this transition is not public! It can be obtained by asking me (in exceptional cases), and it is OK to be used **only** for the purpose outlined here. You are legaly not allowed to copy it or use it for any other purpose. 


I made two short hands-on videos about the step described in this README. The first describes at the example of "corsika7" how we can migrate a large svn project to gitlab: [video1](https://www.youtube.com/watch?v=aVsAfvU6pK4), while the second one illustrates how such a migrated project can be used for development from (1) svn command line, (2) git command line, or (3) gitlab web interface: [video2](https://www.youtube.com/watch?v=W6OwANSIuxc) in a consistent and simultaneous way. 


# Preparation

Install git, subversion, any editor, and git-svn, optional: ccrypt

## Authorship mapping 

From our special "svn-admin repository" (restricted) we can extract an author conversion table in the form
"svn-author-name = Full Name <email>" 
which is then used during the conversion to assign git users with mail to each git commit. We want this in order to preserve as much as possible our development history including the respective authorship. 

The script `authors.py` can be run by admins on the svn admin repository
and will produce the file `authors-devel-ik.txt`. The output file is here
in the repository and should be sufficient for most cases. It is
encrypted, you can decrypt it with 
`ccdecrypt authors-devel-ik.txt.cpt` but you have to ask me for the password and I only distribute it in few controlled cases. 

You also have to extract the list of contributors from a concrete svn project from its "svn log" with:
```
svn log https://devel-ik.fzk.de/svn/mc -q | awk -F '|' '/^r/ {sub("^ ", "", $2); sub(" $", "", $2); print $2" = "$2" <"$2"@from.svn>"}' | sort -u > authors-svn.txt
```
where you have to replace "https://devel-ik.fzk.de/svn/mc" by the repository of your interest. 

In a third step you need to merge `authors-devel-ik.txt` and `authors-svn.txt` using `merge_authors.py`. Just copy all of this in one directory and run the script. The output is `authors.txt` and contains a complete list of authors known to us for a particular repository.

## Create a new, empty repository on our gitlab server

Log into gitlab.ikp.kit.edu, press "new project", select proper "group" and "permissions" etc. 
For this concrete example the generated repository is: `git@gitlab.ikp.kit.edu:AirShowerPhysics/corsika7.git` 
Rememer the git or https location of this. Click "clone" to see.  

# Migrate the data 

## Create a local git mirror of your svn project

Execute
`git svn clone https://devel-ik.fzk.de/svn/mc/corsika -T trunk -t releases -b branches --prefix=svn/ -A authors.txt`

Note: this can take hours. Depending on project age and size. 

If your project follows a standard svn project 
layout (trunk, tags, branches) it is sufficient to replace `-T trunk -t releases -b branches` by just `-s`; if your project has no such internal layout just also drop the `-s`.

## Copy svn-ignores

We want git to ignore the same things as svn:

```
git svn show-ignore > .gitignore
git add .gitignore
git commit -m "copied svn-ignores"
```

## Add svn:externals

Initially the svn:externals are all gone. The obvious way to get them back in git is to 

 1. convert them individuall into git projects first
 2. use git submodules 

 Go to the repository location where you need the external. Use
 `git submodule add repo-link-https-or-git local-dir-name`
 and also don't forget to add 'local-dir-name' to the `.gitignore` to prevent too much automation...

## Create real git tags

If you type `git branch -a` or `git tag --list` you see that currently the svn tags are just branches in git (-> subdirectories in svn). They should be converted into real tags, and the "directories" removed. Note: after removing you won't be able to modify tags from the git-svn checkout any more... but those are tags! Right?? 

`for t in $(git for-each-ref --format='%(refname:short)' refs/remotes/svn/tags); do git tag ${t/tags\//} $t && git branch -D -r $t; done`

Type `git branch -a` and `git tag --list` to see the effect. 

### Create local branches for git 

Check your branch structure with `git branch -a`.

`for b in $(git for-each-ref --format='%(refname:short)' refs/remotes); do git branch $b refs/remotes/$b && git branch -D -r $b; done`

Now branches from svn are git-branches: svn/branchname

But we also have to explain to git-svn about the new mapping of git-branches to svn-branches. Edit ".git/config" and change section
```
[svn-remote "svn"]
	url = https://devel-ik.fzk.de/svn/mc
	fetch = corsika/trunk:refs/remotes/svn/trunk
	branches = corsika/branches/*:refs/remotes/origin/svn/*
```
The "branches" line has to precisely match the svn project layout, here: "url/corsika/branches/branch-name" and the git side needs to reflect the git branch naming scheme, here: "refs/remoted/origin/svn/branch-name". You can type `git branch -a` to get information on the branch naming scheme. 

Note: also remvoe the "tags=" line, since after converting svn tags to git-tags modifying svn-tags is not a good idea (it never was). 

## Optional

Note: optional steps are **not** a good idea if you plan to continue to work with the existing svn project... This is only useful for definitive one-way transition to git.

### rewrite history, rewrite commits

At this stage, the repository is available as full commit history in a local git repository. For a complete move from svn to git (no parallel development) you may drastically modify
the file-structure and also the commit history as you wish. Commits can be dropped, changed, re-arranged. Files (binaries etc.) can be entirely removed from the history, etc. 

### remove large (binary) files

One good use-case here is to remove large binary files from the repository. See git-lfs for good ideas. 


# Publish on gitlab

Now we are ready to add the remote link to our gitlab server. 

`git remote add origin link-to-gitlab-projec-https-or-git`

Where you specify the link copied from the gitlab web interface above.

```
git push --all
git push --tags
```

Will push everthing to gitlab. This can take a while since the repository may be large.  




# Work with git and svn

## track specific svn branch

`git checkout -b svn/branch svn/branch`
Note, the `-b branch` is only needed the first time you ever work on a branch (locally). This just creates a local version of this branch. Just use `git checkout svn/branch` if the local branch already exists. 

## do `svn info`

`git svn info`

## do `svn update`

`git svn rebase`

## do `svn commit`

First commit on git (locally):
```
git add file(s)
git commit -m "message"
```
Then also push this to svn:
```
git svn dcommit
```

(Note: Commiting in git to a server is always a multi-step process! This is considerably different from svn. )

## get git repo updated with new commits

`git commit -am 'message'`

where the `-a` will use all current modification, or

`git add files && git commit -m 'message'` where only specific files are included. Note for specialists: you can even only select line-ranges from individual files to be included in a commit!

Don't forget `git push` to push local commits to the gitlab server, see also `git svn dcommit` above. 

## get latest changes from git

`git pull` or equivallently `git fetch && git merge`

## Where is my "trunk"

In git by default the "trunk" is called "master". 

`git checkout master` puts you on the trunk. Verify with `git svn info` and of course `git status`.

# Further resources

The git cheat sheet: https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet

From svn to git, short notes: https://www.git-tower.com/blog/git-for-subversion-users-cheat-sheet/


From svn to git, long read: https://www.codemag.com/Article/1105101/Git-for-Subversion-Users

