#!/usr/bin/python3
import yaml

file=open('cfg/iksvn_config.yml')
out=open('authors-devel-ik.txt', 'w')
data=yaml.load(file)
users=data['users']
users_None=[]
for user in users:
    if (users[user]['realname']==None or users[user]['email']==None):
        users_None.append(user)
    out.write('{} = {} <{}>\n'.format(user, users[user]['realname'] or user, users[user]['email'] or 'un@kno.wn'))
out.write('(no author) = no_author <no_author@no_author>\n')
out.close()


